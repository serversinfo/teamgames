To do list
==========

- [ ] Add fixed player skins to extras
- [ ] Check reported buggy HE battle - fixed?
- [ ] Add cookie settings for nobullet marks?
- [ ] Create module for custom laser/steam/model/particles marks
- [ ] Fix updater link & file - *not yet*

---

- [x] Rewrite `CreateConfigDirectoryIfNotExist`
- [x] Plugins configs...
- [x] Make wiki for some modules
- [x] Add some ConVars to bombtoss module
- [x] Rewrite bombtoss translation
- [x] Move `extras/screenshots/` to `extras/wiki/`
- [x] Update warden module wiki
- [x] Fix `tg_game_checkteams` in tg menu
- [x] Add some settings to Warden module
- [x] Rename members in MarkStruct
- [x] Allow "spawning" marks without sprite (to allow modules use custom marks as official tg marks)
- [x] Warden - Move color code from src to translations
- [x] Rewrite TG_GetRegedModules (?) - nope
- [x] Add non-ammo spending & damage dealign marks
- [x] Rewrite native TG_ShowPlayerSelectMenu
- [x] Check/Fix option "disabled" in modules config
- [x] Wrap (some) forwards
- [x] Add ConVar `tg_game_checkteams`
- [x] Finish API docs
- [x] Finish wiki
- [x] Add missing download lists to extras
- [x] Fix bug about console winning redonly game
- [x] Fix some log messages
- [x] Fix bug in ChickenHunt about spawning chickens
- [x] Separate translations by langs
- [x] Improve !rebel check messages
- [x] Fix disabling fences on certain maps - wasn't bug
- [x] Add sounds to extras
- [x] Add native TG_GetModuleName
- [x] Add forward TG_AskModuleName and fix module names
- [x] Fix warden module (check for access on menu actions) + add cvars for marks, fence
- [x] Finish warden module
- [x] Rename some translations files
- [x] Add gitignore
- [x] Remove caching for non-string cvars
- [x] Add cvars to set how many frags/score give when prisoner kills another one in game
- [x] Check fence beam heights according to fence corner model - it's ok

