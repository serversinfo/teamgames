#TeamGames

Modular SourceMod plugin system for CS:S/CS:GO JailBreak servers with many features:
- Two [teams](https://github.com/KissLick/TeamGames/wiki/Teams), red and blue
- Team based [games](https://github.com/KissLick/TeamGames/wiki/Games)
- Two different [marks](https://github.com/KissLick/TeamGames/wiki/Marks) (each for one team)
- [Fences](https://github.com/KissLick/TeamGames/wiki/Fences) (reacts only to prisoners and according to server settings)
- Fully in translations files and using [ColorVariables](https://github.com/KissLick/ColorVariables)
- Use [SourceMod override system](https://wiki.alliedmods.net/Overriding_Command_Access_%28SourceMod%29) to [restrict module access](https://github.com/KissLick/TeamGames/wiki/Module-config#overriding-menu-items-access) player by player
- Customize [TeamGames menu](https://github.com/KissLick/TeamGames/wiki/Module-config) as you wish
- Using [[DTC] DownloadTableConfig](https://github.com/KissLick/DownloadTableConfig) to easily handle files and downloads
- Keep [logs](https://github.com/KissLick/TeamGames/wiki/Logs) of TeamGames activities
- [Warden plugin](https://github.com/KissLick/TeamGames/wiki/Warden-plugin) support
- Wide [API](http://www.teamgames.ga/teamgames) for plugin developers

***Found a bug?*** **[Issue](https://github.com/KissLick/TeamGames/issues/new?title=Not%20something%20like%20%22bug%22%20or%20%22problem%22%20pls...&labels=bug) it please, so I can fix it!** Do you know how to improve TeamGames? [Issue](https://github.com/KissLick/TeamGames/issues/new?title=Not%20something%20like%20%22improvement%22%20or%20%22good%20idea%22%20pls...&labels=improvement) you idea ;-)

##Games

**RedOnly:**
- Hot popato
- Drunken Rambo

**FiftyFifty:**
- HE grenades
- Machines + 500 HP
- Cocktail party *(CS:GO only)*

**Both:**
- Chicken hunt *(CS:GO only)*
- Gun fight
- HeadShot only
- Knife fight
- No zoom
- Pistol zoom battle
- Reload battle
- Taser mania *(CS:GO only)*

##Non-game modules
- Warden
- Bomb toss
- Permanent fences
